<?php

/**
 * @file
 * Link apache solr search functionalities to services module.
 */

/**
 * Callback for solr service.
 */
function solr_service_results($fn, $key, $fields, $facets = FALSE, $page = 0) {
  $stdkeys = explode(',', str_replace(' ', '', $fields));
  $results = array();
  watchdog('solr_service', 'search.solr invoked for !keys', array('!keys' => $key));

  try {
    $condition = apachesolr_search_conditions();
    $results = apachesolr_search_search_execute($key, $condition);
  }
  catch (Exception $e) {
    watchdog('solr_service', $e->getMessage(), NULL, WATCHDOG_ERROR);
    apachesolr_failure(t('Solr search'), $key);
  }

  watchdog('solr_service', 'search.solr returned !count results for !keys', array('!count' => count($results), '!keys' => $key));

  if ($results and is_array($results) and count($results)) {
    // If specific fields requested, remove extra data.
    if ($fields) {
      $num = count($results);
      for ($i = 0; $i < $num; $i++) {
        $value = array_keys($results[$i]);
        foreach ($value as $keys) {
          if (!in_array($keys, $stdkeys)) {
            unset($results[$i][$keys]);
          }
        }
      }
    }
    // Result count.
    $env_id = apachesolr_default_environment();
    $query = apachesolr_current_query($env_id);
    if (is_object($query)) {
      $response = apachesolr_static_response_cache($query->getSearcher());
      $results['total'] = $response->response->numFound;
    }
    // If Facets enabled then show.
    if ($facets) {
      $searcher = $query->getSearcher();
      $results['facets'] = facetapi_build_realm($searcher, 'block');
    }
    return $results;
  }
  return services_error(t('Search returned no results.'));
}
