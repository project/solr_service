Apache Solr Search:
===================
This module provides a Services resource that enables
a remote application to search for Apache Solr indexed
content on your site using Drupal core's search and Apache
Solr API's features. It's developed for Services 7.x-3.x.

This Service provide the Solr details including Number of
results, Facets that enabled for this core and we can able
specify fields we want to show.

Requirements
============
1. Services (https://www.drupal.org/project/services)
(The 3.0 version is incompatible with Services 7.x-3.2 and
later. Please use the -dev version and report your results here.)
2. Search API Solr Search
(https://www.drupal.org/project/search_api_solr)
  
Usage
=====

Apache Solr configuration:
--------------------------
 Install Apache Solr API's module by following handbook [1].
 Index the required datas to Solr server.
 
[1] https://www.drupal.org/node/1999280

Service Configuration
---------------------
Create a Service for Solr search using services module by
following handbook [2].
Specify endpoint with a path of your choice, e.g. services.
Choose the REST server.
Edit the endpoint's resources and check search confirm
that retrive is enabled.
Edit the server settings and choose a formatter you want.
  Try the service's URLs in a browser:
  a) Retrive all Details not considering search key word
    http://my.example.com/services/search/retrieve
    for content search.

  b) Retrive data based on keyword
    http://my.example.com/services/search/retrieve?key=KEYWORD
    for content search with key word.

  c) Specifing fields we want
    http://my.example.com/services/search/retrieve?key=KEYWORD
    &fileds=FIELD1
    for content search with key word and single Field.
    http://my.example.com/services/search/retrieve?key=KEYWORD
    &fileds=FIELD1,FIELD2,FIELD3,..
    for content search with key word and Multiple Field.

  c) Specifing Page to show result.
    http://my.example.com/services/search/retrieve?key=KEYWORD
    &fileds=FIELD1&page=1
    for content search with key word, single Field by specifying
    particular page.

  d) Enabling facets
    http://my.example.com/services/search/retrieve?key=KEYWORD
    &facets=1
    for content search with key word and Facet enabled.

[2] https://www.drupal.org/node/109782
